package com.amicly.prettysimplehueapp;

import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhilipsHueLightListWearActivity extends WearableActivity implements
        WearableListView.ClickListener, DataApi.DataListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String LOG_TAG = "LightListActivity";

    /**
     * Request code for launching the Intent to resolve Google Play services errors.
     */
    private static final int REQUEST_RESOLVE_ERROR = 1000;

    private static final String REQUEST_LIGHTS_PATH = "/request-lights";

    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError = false;
    private LightListAdapter mLightListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_philips_hue_light_list);
        setAmbientEnabled();

        WearableListView listView = (WearableListView) findViewById(R.id.wearable_list);
        mLightListAdapter = new LightListAdapter(PhilipsHueLightListWearActivity.this, new ArrayList<String>(0));
        listView.setAdapter(mLightListAdapter);
        listView.setClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
            Log.d(LOG_TAG, "Connected to Google Api Service");
            Wearable.DataApi.addListener(mGoogleApiClient, this);
            Wearable.NodeApi.getConnectedNodes(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                @Override
                public void onResult(@NonNull NodeApi.GetConnectedNodesResult nodes) {
                    for (Node node : nodes.getNodes()) {
                        Wearable.MessageApi.
                                sendMessage(mGoogleApiClient, node.getId(), REQUEST_LIGHTS_PATH,
                                        "sending a message".getBytes())
                                .setResultCallback(
                                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void
                            onResult(@NonNull MessageApi.SendMessageResult sendMessageResult) {
                                if (sendMessageResult.getStatus().isSuccess()) {
                                    Log.d(LOG_TAG, "Message successfully sent");
                                }

                            }
                        });
                    }

                }
            });


        Wearable.DataApi.getDataItems(mGoogleApiClient).setResultCallback(new ResultCallback<DataItemBuffer>() {
            @Override
            public void onResult(@NonNull DataItemBuffer dataItems) {
                for (int i = 0; i < dataItems.getCount(); i++) {
                    if (dataItems.get(i).getUri().getPath().equals("/lights")){
                        ArrayList<String> lightList =
                                DataMapItem.fromDataItem(dataItems.get(i))
                                        .getDataMap().getStringArrayList("lightList");
                        Log.d(LOG_TAG, "Got the following string array" + Arrays.toString(
                                lightList.toArray()));
                        mLightListAdapter
                                .replaceData(lightList);
                    }
                }
            }
        });

    }



    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(LOG_TAG, "onConnectionSuspended: " + cause);
    }

    @Override //OnConnectionFailedListener
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            Log.e(LOG_TAG, "Connection to Google API client has failed");
            mResolvingError = false;
//            mStartActivityBtn.setEnabled(false);
//            mSendPhotoBtn.setEnabled(false);
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
//            Wearable.MessageApi.removeListener(mGoogleApiClient, this);
//            Wearable.NodeApi.removeListener(mGoogleApiClient, this);
        }
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                // DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/lights") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    Log.d(LOG_TAG, "Got the following string array" + Arrays.toString(dataMap.getStringArrayList("lightList").toArray()));
                    mLightListAdapter.setList(dataMap.getStringArrayList("lightList"));
                }
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                // DataItem deleted
            }
        }
    }

    @Override
    public void onClick(WearableListView.ViewHolder v) {
//        updateNotification((Integer) v.itemView.getTag());
    }

    @Override
    public void onTopEmptyRegionClick() {
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
//        updateDisplay();
    }

    @Override
    public void onUpdateAmbient() {
        super.onUpdateAmbient();
//        updateDisplay();
    }

    @Override
    public void onExitAmbient() {
//        updateDisplay();
        super.onExitAmbient();
    }

//    private void updateDisplay() {
//        if (isAmbient()) {
//            mContainerView.setBackgroundColor(getResources().getColor(android.R.color.black));
//            mTextView.setTextColor(getResources().getColor(android.R.color.white));
//            mClockView.setVisibility(View.VISIBLE);
//
//            mClockView.setText(AMBIENT_DATE_FORMAT.format(new Date()));
//        } else {
//            mContainerView.setBackground(null);
//            mTextView.setTextColor(getResources().getColor(android.R.color.black));
//            mClockView.setVisibility(View.GONE);
//        }
//
//    }

    private static final class LightListAdapter extends WearableListView.Adapter {
        private final Context mContext;
        private final LayoutInflater mInflater;
        private List<String> mLights;

        private LightListAdapter(Context context, List<String> lights) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
            setList(lights);
        }

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new WearableListView.ViewHolder(
                    mInflater.inflate(R.layout.light_list_item, null));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder holder, int position) {
            TextView view = (TextView) holder.itemView.findViewById(R.id.name);
            view.setText(mLights.get(position));
            holder.itemView.setTag(position);
        }

        @Override
        public int getItemCount() {
            return mLights.size();
        }

        public void replaceData(List<String> lights) {
            setList(lights);
            notifyDataSetChanged();
        }

        private void setList(List<String> lights) {
            mLights = lights;
        }
    }
}
