package com.amicly.prettysimplehueapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.amicly.prettysimplehueapp.data.HueSharedPreferences;
import com.amicly.prettysimplehueapp.data.LightListAdapter;
import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;

import java.util.List;

public class PhilipsHueLightListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private PHHueSDK phHueSDK;
    public static final String TAG = "PrettySimpleHueApp";
    private HueSharedPreferences prefs;
    private LightListAdapter adapter;
    public static String LIGHT_EXTRA;
    private ListView lightList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_philips_hue_light_list);

        phHueSDK = PHHueSDK.create();
        PHBridge bridge = phHueSDK.getSelectedBridge();
        phHueSDK.enableHeartbeat(bridge, PHHueSDK.HB_INTERVAL);

        List<PHLight> allLights = bridge.getResourceCache().getAllLights();
        adapter = new LightListAdapter(getApplicationContext(), allLights);

        lightList = (ListView) findViewById(R.id.light_list);
        lightList.setOnItemClickListener(this);
        lightList.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Toast.makeText(PhilipsHueLightListActivity.this, "A light was clicked", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), PhilipsHueLightActivity.class);
        intent.putExtra(LIGHT_EXTRA, ((PHLight) adapter.getItem(position)).getUniqueId());
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PHBridge bridge = phHueSDK.getSelectedBridge();
        if (bridge != null) {

            if (phHueSDK.isHeartbeatEnabled(bridge)) {
                phHueSDK.disableHeartbeat(bridge);
            }

            phHueSDK.disconnect(bridge);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();

    }
}
