package com.amicly.prettysimplehueapp.utility;

import android.graphics.Color;

import com.philips.lighting.hue.listener.PHLightListener;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by daz on 11/27/15.
 */
public class PhilipsHueLightUtilities {


    public static void toggleLight(PHBridge bridge, PHLight lightToUpdate, boolean state, PHLightListener listener) {

        PHLightState lightState = new PHLightState();
        lightState.setOn(state);
        bridge.updateLightState(lightToUpdate, lightState, listener);
    }

    public static void toggleLight(PHLight lightToUpdate, boolean state) {

        PHBridge bridge = PHHueSDK.create().getSelectedBridge();
        PHLightState lightState = new PHLightState();
        lightState.setOn(state);
        bridge.updateLightState(lightToUpdate, lightState);
    }

    public static void setColor(PHBridge bridge, PHLight lightToUpdate, Integer color, PHLightListener listener) {

        ArrayList<Float> xyArray = getRGBtoXY(color);
        PHLightState lightState = new PHLightState();
        lightState.setX(xyArray.get(0), true);
        lightState.setY(xyArray.get(1), true);
        bridge.updateLightState(lightToUpdate, lightState, listener);
    }

    public static ArrayList<Float> getRGBtoXY(Integer c) {
        // For the hue bulb the corners of the triangle are:
        // -Red: 0.675, 0.322
        // -Green: 0.4091, 0.518
        // -Blue: 0.167, 0.04
        double[] normalizedToOne = new double[3];
        float colorRed, colorGreen, colorBlue;
        colorRed = Color.red(c);
        colorGreen = Color.green(c);
        colorBlue = Color.blue(c);
        normalizedToOne[0] = (colorRed / 255);
        normalizedToOne[1] = (colorGreen / 255);
        normalizedToOne[2] = (colorBlue / 255);
        float red, green, blue;

        // Make red more vivid
        if (normalizedToOne[0] > 0.04045) {
            red = (float) Math.pow(
                    (normalizedToOne[0] + 0.055) / (1.0 + 0.055), 2.4);
        } else {
            red = (float) (normalizedToOne[0] / 12.92);
        }

        // Make green more vivid
        if (normalizedToOne[1] > 0.04045) {
            green = (float) Math.pow((normalizedToOne[1] + 0.055)
                    / (1.0 + 0.055), 2.4);
        } else {
            green = (float) (normalizedToOne[1] / 12.92);
        }

        // Make blue more vivid
        if (normalizedToOne[2] > 0.04045) {
            blue = (float) Math.pow((normalizedToOne[2] + 0.055)
                    / (1.0 + 0.055), 2.4);
        } else {
            blue = (float) (normalizedToOne[2] / 12.92);
        }

        float X = (float) (red * 0.649926 + green * 0.103455 + blue * 0.197109);
        float Y = (float) (red * 0.234327 + green * 0.743075 + blue * 0.022598);
        float Z = (float) (red * 0.0000000 + green * 0.053077 + blue * 1.035763);

        float x = X / (X + Y + Z);
        float y = Y / (X + Y + Z);

        double[] xy = new double[2];
        xy[0] = x;
        xy[1] = y;
//        ArrayList<Float> xyAsList = new ArrayList<Float>();
        ArrayList<Float> xyAsList = new ArrayList<Float>(Arrays.asList(new Float[]{x, y}));
        return xyAsList;
    }






}
