package com.amicly.prettysimplehueapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

import com.amicly.prettysimplehueapp.data.HueSharedPreferences;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.philips.lighting.hue.listener.PHLightListener;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeResource;
import com.philips.lighting.model.PHHueError;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import java.util.List;
import java.util.Map;

import static com.amicly.prettysimplehueapp.utility.PhilipsHueLightUtilities.setColor;
import static com.amicly.prettysimplehueapp.utility.PhilipsHueLightUtilities.toggleLight;

public class PhilipsHueLightActivity extends AppCompatActivity {

    private PHHueSDK phHueSDK;
    public static final String TAG = "PrettySimpleHueApp";
    private HueSharedPreferences prefs;
    private PHBridge bridge;
    PHLight lightToUpdate = null;

    private ColorPicker mColorPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_philips_hue_light);

        String lightId = getIntent().getExtras().getString(PhilipsHueLightListActivity.LIGHT_EXTRA);

        phHueSDK = PHHueSDK.create();
        bridge = phHueSDK.getSelectedBridge();
        phHueSDK.enableHeartbeat(bridge, PHHueSDK.HB_INTERVAL);
        List<PHLight> allLights = bridge.getResourceCache().getAllLights();


        for (PHLight light : allLights) {
            if (light.getUniqueId().equals(lightId)){
               lightToUpdate = light;
            }
        }

        getSupportActionBar().setTitle(lightToUpdate.getName());

        final Switch lightSwitch = (Switch) findViewById(R.id.light_switch);
        lightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                  toggleLight(bridge, lightToUpdate, isChecked, listener);
            }
        });
        lightSwitch.setChecked(lightToUpdate.getLastKnownLightState().isOn());

        SeekBar brightnessSeekbar = (SeekBar) findViewById(R.id.light_brightness_seekbar);
        brightnessSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d(TAG,"Progress is: "+progressChanged);
                updateLight(progressChanged);
                lightSwitch.setChecked(true);
            }
        });

        int progressToSet = (int) (lightToUpdate.getLastKnownLightState().getBrightness() / 2.55);
        brightnessSeekbar.setProgress(progressToSet);

        mColorPicker = (ColorPicker) findViewById(R.id.colorpicker);
        mColorPicker.setOnColorSelectedListener(new ColorPicker.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                setColor(bridge, lightToUpdate, color, listener);
                lightSwitch.setChecked(true);
            }
        });

        if (null == lightToUpdate.getLastKnownLightState().getColorMode().getValue() ) {
            mColorPicker.setVisibility(View.GONE);
        }


    }

    private void updateLight(int brightness) {

        PHLightState lightState = new PHLightState();
        lightState.setOn(true);
        lightState.setBrightness((int)(brightness * 2.55));
        bridge.updateLightState(lightToUpdate, lightState, listener);
    }

    // If you want to handle the response from the bridge, create a PHLightListener object.
    PHLightListener listener = new PHLightListener() {

        @Override
        public void onSuccess() {
        }

        @Override
        public void onStateUpdate(Map<String, String> arg0, List<PHHueError> arg1) {
            Log.w(TAG, "Light successfully updated");
        }

        @Override
        public void onError(int arg0, String arg1) {}

        @Override
        public void onReceivingLightDetails(PHLight arg0) {}

        @Override
        public void onReceivingLights(List<PHBridgeResource> arg0) {}

        @Override
        public void onSearchComplete() {}
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PHBridge bridge = phHueSDK.getSelectedBridge();
        if (bridge != null) {

            if (phHueSDK.isHeartbeatEnabled(bridge)) {
                phHueSDK.disableHeartbeat(bridge);
            }

            phHueSDK.disconnect(bridge);
        }
    }
}
