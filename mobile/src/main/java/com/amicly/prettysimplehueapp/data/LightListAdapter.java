package com.amicly.prettysimplehueapp.data;

import android.content.Context;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.amicly.prettysimplehueapp.R;
import com.philips.lighting.model.PHLight;


import java.util.List;

import static com.amicly.prettysimplehueapp.utility.PhilipsHueLightUtilities.toggleLight;

/**
 * Created by daz on 11/21/15.
 */
public class LightListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<PHLight> lights;

    class LightListItem {
        private TextView lightName;
        private TextView lightState;
        private Switch lightSwitch;
    }

    /**
     * creates instance of {@link LightListAdapter} class.
     *
     * @param context     the Context object.
     * @param lights      an array list of {@link PHLight} object to display.
     */
    public LightListAdapter(Context context, List<PHLight> lights) {
        // Cache the LayoutInflate to avoid asking for a new one each time.
        mInflater = LayoutInflater.from(context);
        this.lights = lights;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LightListItem item;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.selectlight_item, null);

            item = new LightListItem();
            item.lightName = (TextView) convertView.findViewById(R.id.light_name);
//            item.lightState = (TextView) convertView.findViewById(R.id.light_status);
            item.lightSwitch = (Switch) convertView.findViewById(R.id.light_switch);


            convertView.setTag(item);
        } else {
            item = (LightListItem) convertView.getTag();
        }

        final PHLight light = lights.get(position);
        item.lightName.setTextColor(Color.BLACK);
        item.lightName.setText(light.getName());
//        String lightStateText = "Status: " +  (light.getLastKnownLightState().isOn() ? "On" : "Off");
//        item.lightState.setTextColor(Color.DKGRAY);
//        item.lightState.setText(lightStateText);
        item.lightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    toggleLight(light, isChecked);
//                    item.lightState.setText("Status: " +  (isChecked ? "On" : "Off"));
            }
        });

        item.lightSwitch.setChecked(light.getLastKnownLightState().isOn());

        return convertView;
    }

    @Override
    public int getCount() {
        return lights.size();
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position  The row index.
     * @return          The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position      Position of the item whose data we want within the adapter's data set.
     * @return              The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return lights.get(position);
    }


}
