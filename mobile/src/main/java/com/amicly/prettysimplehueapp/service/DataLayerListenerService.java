package com.amicly.prettysimplehueapp.service;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.amicly.prettysimplehueapp.data.HueSharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by daz on 12/26/15.
 */
public class DataLayerListenerService extends WearableListenerService {

    private static final String START_ACTIVITY_PATH = "/start-activity";
    private static final String DATA_ITEM_RECEIVED_PATH = "/data-item-received";
    private static final String REQUEST_LIGHTS_PATH = "/request-lights";

    private PHHueSDK phHueSDK;
    public static final String TAG = "DataLayerListener";
    private HueSharedPreferences prefs;
    private PHBridge bridge;
    private GoogleApiClient mGoogleApiClient;
    private List<PHLight> mAllLights;
    private ArrayList<String> lightList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        phHueSDK = PHHueSDK.create();
        bridge = phHueSDK.getSelectedBridge();
        phHueSDK.enableHeartbeat(bridge, PHHueSDK.HB_INTERVAL);
        mAllLights = bridge.getResourceCache().getAllLights();

        for (PHLight light : mAllLights) {
            lightList.add(light.getName());
        }


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDataChanged: " + dataEvents);
        }
        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);

        ConnectionResult connectionResult =
                mGoogleApiClient.blockingConnect(30, TimeUnit.SECONDS);

        if (!connectionResult.isSuccess()) {
            Log.e(TAG, "Failed to connect to GoogleApiClient.");
            return;
        }

        // Loop through the events and send a message
        // to the node that created the data item.
        for (DataEvent event : events)  {
            Uri uri = event.getDataItem().getUri();

            // Get the node id from the host value of the URI
            String nodeId = uri.getHost();
            // Set the data of the message to be the bytes of the URI
            byte[] payload = uri.toString().getBytes();

            // Send the RPC
            Wearable.MessageApi.sendMessage(mGoogleApiClient, nodeId,
                    DATA_ITEM_RECEIVED_PATH, payload);
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equals(REQUEST_LIGHTS_PATH)) {
            PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/lights");
            putDataMapReq.getDataMap().putStringArrayList("lightList", lightList);
            PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
            putDataReq.setUrgent();
            Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq).setResultCallback(
                    new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
                            if(dataItemResult.getStatus().isSuccess()) {
                                Log.d(TAG,"Successfully put lightList data item!");
                            }
                        }
                    }
            );

        }
    }
}
